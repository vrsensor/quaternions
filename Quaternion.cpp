
#include "Quaternion.h"

/*

 #####                                                           
#     # #    #   ##   ##### ###### #####  #    # #  ####  #    # 
#     # #    #  #  #    #   #      #    # ##   # # #    # ##   # 
#     # #    # #    #   #   #####  #    # # #  # # #    # # #  # 
#   # # #    # ######   #   #      #####  #  # # # #    # #  # # 
#    #  #    # #    #   #   #      #   #  #   ## # #    # #   ## 
 #### #  ####  #    #   #   ###### #    # #    # #  ####  #    # 
 
*/

Quaternion::Quaternion(float w, float x, float y, float z)
{
	w_ = w;
	x_ = x;
	y_ = y;
	z_ = z;
	length = this->GetLength();
}

Quaternion::Quaternion(float angle, Vector3f axis)
{
	angle *= DEG2RAD / 2.0;
	float sinAngle = sin(angle );
	float cosAngle = cos(angle);
	w_ = cosAngle;
	x_ = axis.x_ * sinAngle;
	y_ = axis.y_ * sinAngle;
	z_ = axis.z_ * sinAngle;
	length = this->GetLength();
}

Quaternion::Quaternion(float x, float y, float z)
{
	w_ = 0.0;
	x_ = x;
	y_ = y;
	z_ = z;
	length = this->GetLength();
}

Quaternion::Quaternion(Vector3f p)
{
	w_ = 0.0;
	x_ = p.x_;
	y_ = p.y_;
	z_ = p.z_;
	length = this->GetLength();
}

void Quaternion::Normalize()
{
	float length = this->GetLength();
	if(isfinite(length) and length >= 1E-9)
	{
		w_ /= length;
		x_ /= length;
		y_ /= length;
		z_ /= length;
		length = this->GetLength();
	}
}

Quaternion Quaternion::operator +(Quaternion q)
{
	return(Quaternion(w_ + q.w_, x_ + q.x_, y_ + q.y_, z_ + q.z_));
}

Quaternion Quaternion::operator -(Quaternion q)
{
	return(Quaternion(w_ - q.w_, x_ - q.x_, y_ - q.y_, z_ - q.z_));
}

Quaternion Quaternion::operator *(Quaternion q)
{
	float w, i, j, k;
	
	w = w_ * q.w_ - x_ * q.x_ - y_ * q.y_ - z_ * q.z_;
	i = w_ * q.x_ + q.w_ * x_ + y_ * q.z_ - q.y_ * z_;
	j = w_ * q.y_ + q.w_ * y_ + z_ * q.x_ - q.z_ * x_;
	k = w_ * q.z_ + q.w_ * z_ + x_ * q.y_ - q.x_ * y_;
	
	return(Quaternion(w, i, j, k));
}


Vector3f Quaternion::toEulerAngles()
{
	Vector3f angles = Vector3f();
	
	// Calcul de l'angle de roulis (roll) autour de l'axe X
	angles.x_ = atan2(2 * (w_ * x_ + y_ * z_), 1 - 2 * (x_ * x_ + y_ * y_));
	
	// Calcul de l'angle de tangage (pitch) autour de l'axe Y
	double r = 2 * (w_ * y_ - z_ * x_);
	if(fabs(r) >= 1.0)
		angles.y_ = copysign(M_PI_2, r);
	else
		angles.y_ = asin(r);
		
	// Calcul de l'angle de lacet (yaw ou heading) autour l'axe Z
	angles.z_ = atan2(2 * (w_ * z_ + x_ * y_), 1 - 2 * (y_ * y_ + z_ * z_));
	
	return(angles);
}


Vector3f Quaternion::Rotate(Vector3f p)
{
	// Créer un quaternion à partir du point spécifié
	Quaternion point = Quaternion(p);

	// Formule de rotation
	// p' = q × p × q(-1)
	Quaternion r = (*this * point * this->Inverse());
	
	// Renvoi des composantes imaginaires (résultat de la rotation)
	return(Vector3f(r.x_, r.y_, r.z_));
}

Quaternion Quaternion::Inverse()
{
	float l = (w_ * w_) + (x_ * x_) + (y_ * y_) + (z_ * z_);
	return(Quaternion(w_ / l, -x_ / l, -y_ / l, -z_ / l));
}

String Quaternion::toString()
{
	return(String("(") + String(w_) + String(" ; ") + String(x_) + String(" ; ") + String(y_) + String(" ; ") + String(z_) + String("), length = ") + String(length));
}


float Quaternion::GetLength()
{
	length = sqrt(w_ * w_ + x_ * x_ + y_ * y_ + z_ * z_);
	return(length);
}
