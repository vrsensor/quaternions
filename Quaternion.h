#ifndef __QUATERNION_H__
#define __QUATERNION_H__

#if ARDUINO < 100
#include <WProgram.h>
#else
#include <Arduino.h>
#endif
#include <math.h>
#include <Vector3f.h>


#ifndef DEG2RAD
#define DEG2RAD	0.017453293
#endif

#ifndef RAD2DEG
#define RAD2DEG 57.295779513
#endif


/*!
 *  \class  Quaternion
 *
 *  \brief Quaternion class used to handle quaternions for computing rotations.
 *
 * That class implements the quaternion functionality. Its main goal is to compute any
 * rotations in the 3D space without any issue related to the gimbal lock phenomenon.
 *
 */
class Quaternion
{
	public:
		/*!
		 * \fn Quaternion(float w, float x, float y, float z)
		 * \brief Builds a quaternion based on its direct values.
		 * \param w the real part
		 * \param x the imaginary part on the X axis
		 * \param y the imaginary part on the Y axis
		 * \param z the imaginary part on the Z axis
		 */
		Quaternion(float w, float x, float y, float z);

		/*!
		 * \fn Quaternion(float angle, Vector3f axis)
		 * \brief Builds a quaternion based on a rotation angle and an axis.
		 * \param angle an angle in degrees ranging from 0 to 360.
		 * \param axis a Vector3f axis (check Vector3f documentation). Such axis can be a normalized
		 *             axis (X, Y or Z) or any arbitrary axis defined by a Vector3f vector.
		 */
		Quaternion(float angle, Vector3f axis);

		/*!
		 * \fn Quaternion(float x, float y, float z);
		 * \brief Builds a quaternion based on a point.

		 * In case a quaternion is built using that constructor, it is defined by a point in the 3D space
		 * and the imaginary parameters are the coordinates of such point. The real part is null.
		 * \param x the point position on the X axis
		 * \param y the point position on the Y axis
		 * \param z the point position on the Z axis
		 */
		Quaternion(float x, float y, float z);

		/*!
		 * \fn Quaternion(Vector3f p);
		 * \brief Builds a quaternion from a point given as a Vector3f type.

		 * In that case, the real part of the quaternion is equal to null.
		 * \param p a point as a Vector3f type
		 */
		Quaternion(Vector3f p);

		/*!
		 * \fn Normalize()
		 * \brief Normalize a quaternion.
		 * This method sets the quaternion length to 1.
		 */
		void Normalize();

		/*!
		 * \fn Inverse()
		 * \brief Returns the quaternion inverse.
		 */
		Quaternion Inverse();

		// Addition de quaternions.
		/*!
		 * \fn operator +(Quaternion q)
		 * \brief Add a quaternion to the current one.
		 *
		 */
		Quaternion operator +(Quaternion q);

		// Soustraction de quaternions.
		Quaternion operator -(Quaternion q);

		// Multiplication de quaternions.
		Quaternion operator *(Quaternion q);

		/*!
		 * \fn toEulerAngles();
		 * \brief Conversion to Euler angles.
		 * \return Returns a Vector3f in which the three coordinates are the roll,
		 *                 pitch and yaw (heading) angles around respectively the X,
		 *                 Y and Z axis.
		 */
		Vector3f toEulerAngles();

		/*!
		 * \fn Rotate(Vector3f p)
		 * \brief Rotate a point
		 *
		 * Rotate the provided point (as a Vector3f type) using the current quaternion.
		 *
		 * \param p	a point in the 3D space represented by a Vector3f type value
		 * 
		 * \return Returns a Vector3f representing the rotated point.
		 * 
		 * Here is below an example of code snippet showing how to use a quaternion to rotate an arbitrary
		 * point by an angle of 30 degrees arount the X axis (forward axis). After the operation, the
		 * 'rotated_point' variable (a Vector3f type) contains the coordinates of the 'point' rotated by
		 * 30 degrees.
		 * 
		 * \code
		 * …
		 * float angle = 30.0; // The rotation angle
		 * Quaternion q = Quaternion(angle, Vector3f::FORWARD); // The quaternion that will be used for rotating any point by 30 degrees around X axis
		 * Vector3f point = Vector3f(1.0, -5.0, 12.0); // An arbitrary point into the 3D space
		 * Vector3f rotated_point = q.Rotate(point); // 'rotated_point' is the 'point' rotated by 30 degrees around X axis (forward axis)
		 * …
		 * \endcode
		 */
		Vector3f Rotate(Vector3f p);

		/*!
		 * \fn toString()
		 * \brief Return a String containing a text based representation of the quaternion.
		 *
		 * That function can be used to display the quaternion values or to send them to a serial line.
		 *
		 * \return Returns a String containing the displayable representation of the quaternion.
		 */
		String toString();

		float w_;	/*!< The real part of the quaternion */
		float x_;	/*!< The X axis coordinate of the quaternion (imaginary part) */
		float y_;	/*!< The Y axis coordinate of the quaternion (imaginary part) */
		float z_;	/*!< The Z axis coordinate of the quaternion (imaginary part) */

	private:
		float GetLength();
		float length;
};

#endif
